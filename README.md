# ugo

## What?

This is a POSIX shell script to ***u***pdate ***go***lang to the latest stable release.

## How?

Download the script:

```sh
sudo wget https://codeberg.org/gprst/ugo/raw/branch/main/ugo
```

Make it executable, and move it to a directory that's in your `PATH`:

```sh
sudo chmod +x ugo
sudo mv ugo /usr/local/bin
```

Run:

```sh
sudo ugo
```

## License

This is free and unencumbered software released into the public domain. See [license](LICENSE).
